var partnerSwiper = new Swiper ('.partner-swiper', {
    // Optional parameters
    direction: 'horizontal',
    loop: true,
    slidesPerView: 4,
    centeredSlides: true,
    paginationClickable: true,
    spaceBetween: 250,
    autoplay: 1,
    speed: 3000,
    loopedSlides: 4,
    freeMode: true,
    freeModeMomentum: false,
    simulateTouch: false,
    watchSlidesVisibility: true,
    breakpoints: {
    // when window width is <= 320px
    320: {
      slidesPerView: 2,
      spaceBetween: 20
    },
    // when window width is <= 480px
    480: {
      slidesPerView: 2,
      spaceBetween: 25
    },
    // when window width is <= 640px
    640: {
      slidesPerView: 3,
      spaceBetween: 30
    },
    // when window width is <= 640px
    991: {
      slidesPerView: 3,
      spaceBetween: 35
    }

  }
})

jQuery(function($) {
  $( window ).on("resize", function(){
    partnerSwiper;
  });
});