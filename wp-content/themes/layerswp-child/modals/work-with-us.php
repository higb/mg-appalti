<section id="modal-wwu container ">
    <div class="section-title large">
        <h3 class="heading text-center">Lavora con noi</h3>
    </div>
    <div class="grid">
        <div class="column span-4">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
            </p>
            <div class="text-center">
                <img src="/wp-content/themes/layerswp-child/assets/images/workman.png" alt="work man" />
            </div>
        </div>
        <div class="column span-8">

               <form class="form-row large grid" method="post" action="/wp-content/themes/layerswp-child/modals/mail_form.php" id="send-mail-lavoraconnoi" >
                    <div class="column span-6">
                            <label for="fname">Nome:</label>
                            <input type="text" name="fname" id="fname"><br>
                            <label for="sname">Cognome:</label>
                            <input type="text" name="sname" id="sname"><br>
                            <label for="age">Età:</label>
                            <select name="age" id="age">
                                <option value="15-20">15-20</option>
                                <option value="21-25">21-25</option>
                                <option value="26-30">26-30</option>
                                <option value="30-35">30-35</option>
                            </select><br>
                    </div>
                    <div class="column span-5">
                            <label for="profession">Professione:</label>
                            <input type="text" name="profession" id="profession"><br>
                            <label for="phone">Telefono</label>
                            <input type="text" name="phone" id="phone"><br>
                            <label for="itsemail">Email</label>
                            <input type="text" name="itsemail" id="itsemail"><br>
                            <label for="other">Curriculum</label>
                            
                    </div>
                    <div class="column span-12 text-center">
                        <a href="javascript:void(0);" onClick="return invia_form_lavoraconnoi();" rel:"modal:open">
                        	<input name="Invia" type="submit" value="Invia" class="button" />
                        </a>
                    </div>
               </form>
            
            
        </div>
    </div>
    


</section>
