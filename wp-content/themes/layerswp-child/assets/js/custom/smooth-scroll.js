jQuery(function($) {

  var $root = $('html, body');
  var navHeight = $(".header-site").height();
  var nameAnchor = $('a[name] + div');

  function setscrollspyfn() {

    // for each element with the class 'color'
     nameAnchor.each( function(i, element) {
        // cache the jQuery object
        var $this = $(this);
        var position = $this.position();
        var anchorGet = $this.prev().attr('name');

        $.scrollSpy(element).on('scrollSpy:enter', function() {
          $('.menu-item a[href$="' + anchorGet + '"]').addClass("selected");
          $('.menu-item a[href]').not($('.menu-item a[href$="' + anchorGet + '"]')).removeClass('selected');
        });
    });
  }

setscrollspyfn();


//   $(window).scroll(function() {
//     if (varble == true) {
//     clearTimeout(culo);
//     varble = false;
//     console.log(varble, " inside if");
//   }
// });

// // 1. Call helloCatAsync passing a callback function,
// //    which will be called receiving the result from the async operation
// helloCatAsync(function(result) {
//     // 5. Received the result from the async function,
//     //    now do whatever you want with it:
//     alert(result);
// });

// // 2. The "callback" parameter is a reference to the function which
// //    was passed as argument from the helloCatAsync call
// function helloCatAsync(callback) {
//     // 3. Start async operation:
//     setTimeout(function() {
//         // 4. Finished async operation,
//         //    call the callback passing the result as argument
//         callback('Nya');
//     }, Math.random() * 2000);
// }


  $('.menu-item a').not($('.menu-item-319 a')).click( function(event, cb) {

    event.preventDefault();
    var moduleName = $('[name="' + $(this).attr('href').substr(1) + '"]');
    var moduleOffset = ( moduleName.offset().top - navHeight );

    setTimeout(( function() {
        $root.animate({
        scrollTop: moduleOffset  },
        500
      );

    }), 250);



    $('.menu-item a').not(this).removeClass('selected');
    $(this).addClass("selected");

    if ( $( window ).width() < 992 ) {
      $('#off-canvas-right').removeClass('open');
    }
  });
  
  

  $('#layers-widget-slide-1 .copy-container .button').click( function(event) {

    event.preventDefault();
    var moduleName = $('[name="' + $(this).attr('href').substr(1) + '"]');
    var moduleOffset = (moduleName.offset().top) - navHeight;

    setTimeout((function() {
        $root.animate({
        scrollTop: moduleOffset  },
        500
      );
    }), 250);
  
  });


 

});
