/**
 * SVGInjector v2.0.26 - Fast, caching, dynamic inline SVG DOM injection library
 * https://github.com/flobacher/SVGInjector2
 * forked from:
 * https://github.com/iconic/SVGInjector
 *
 * Copyright (c) 2015 flobacher <flo@digital-fuse.net>
 * @license MIT
 *
 * original Copyright (c) 2014 Waybury <hello@waybury.com>
 * @license MIT
 */
!function(e,t){"use strict";var n=function(){function n(e){n.instanceCounter++,this.init(e)}var r,s,i,l,a,o,c,u,f,p,d,m,g,v,h,y,b,S,A,C,x,N,k,w,j,E,I,T,G,V="http://www.w3.org/2000/svg",F="http://www.w3.org/1999/xlink",O="sprite",L=O+"--",q=[O],M="icon";return i=[],n.instanceCounter=0,n.prototype.init=function(n){n=n||{},r={},o={},o.isLocal="file:"===e.location.protocol,o.hasSvgSupport=t.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#BasicStructure","1.1"),s={count:0,elements:[]},l={},a={},a.evalScripts=n.evalScripts||"always",a.pngFallback=n.pngFallback||!1,a.onlyInjectVisiblePart=n.onlyInjectVisiblePart||!0,a.keepStylesClass="undefined"==typeof n.keepStylesClass?"":n.keepStylesClass,a.spriteClassName="undefined"==typeof n.spriteClassName?O:n.spriteClassName,a.spriteClassIdName="undefined"==typeof n.spriteClassIdName?L:n.spriteClassIdName,a.removeStylesClass="undefined"==typeof n.removeStylesClass?M:n.removeStylesClass,a.removeAllStyles="undefined"!=typeof n.removeAllStyles&&n.removeAllStyles,a.fallbackClassName="undefined"==typeof n.fallbackClassName?q:n.fallbackClassName,a.prefixStyleTags="undefined"==typeof n.prefixStyleTags||n.prefixStyleTags,a.spritesheetURL="undefined"!=typeof n.spritesheetURL&&""!==n.spritesheetURL&&n.spritesheetURL,a.prefixFragIdClass=a.spriteClassIdName,a.forceFallbacks="undefined"!=typeof n.forceFallbacks&&n.forceFallbacks,a.forceFallbacks&&(o.hasSvgSupport=!1),x(t.querySelector("html"),"no-svg",o.hasSvgSupport),o.hasSvgSupport&&"undefined"==typeof n.removeStylesClass&&C(a.removeStylesClass)},n.prototype.inject=function(e,t,n){if(void 0!==e.length){var r=0,s=this;I.call(e,function(i){s.injectElement(i,function(s){n&&"function"==typeof n&&n(s),t&&e.length===++r&&t(r)})})}else e?this.injectElement(e,function(r){n&&"function"==typeof n&&n(r),t&&t(1),e=null}):t&&t(0)},n.prototype.injectElement=function(e,t){var n,r=e.getAttribute("data-src")||e.getAttribute("src");if(!r){if(!a.spritesheetURL)return;if(n=h(e),""===n)return;r=a.spritesheetURL+"#"+n}e.setAttribute("data-src",r);var i=r.split("#");1===i.length&&i.push("");var l;if(!/\.svg/i.test(r))return void t("Attempted to inject a file with a non-svg extension: "+r);if(!o.hasSvgSupport){var f=e.getAttribute("data-fallback")||e.getAttribute("data-png");return void(f?(e.setAttribute("src",f),t(null)):a.pngFallback?(i.length>1&&i[1]?(l=i[1]+".png",j(a.fallbackClassName)?c(e,i[1],a.fallbackClassName):w(a.fallbackClassName)?a.fallbackClassName(e,i[1]):"string"==typeof a.fallbackClassName?E(e,a.fallbackClassName):e.setAttribute("src",a.pngFallback+"/"+l)):(l=r.split("/").pop().replace(".svg",".png"),e.setAttribute("src",a.pngFallback+"/"+l)),t(null)):t("This browser does not support SVG and no PNG fallback was defined."))}j(a.fallbackClassName)&&u(e,i[1],a.fallbackClassName),s.elements.indexOf(e)===-1&&(s.elements.push(e),A(t,r,e))},n.prototype.getEnv=function(){return o},n.prototype.getConfig=function(){return a},c=function(e,t,n){var r="undefined"==typeof n?q:n.slice(0);I.call(r,function(e,n){r[n]=e.replace("%s",t)}),E(e,r)},u=function(e,t,n){n="undefined"==typeof n?q.slice(0):n.slice(0);var r,s,i=e.getAttribute("class");"undefined"!=typeof i&&null!==i&&(s=i.split(" "),s&&(I.call(n,function(e){e=e.replace("%s",t),r=s.indexOf(e),r>=0&&(s[r]="")}),e.setAttribute("class",k(s.join(" ")))))},p=function(e,t,n,r){var s=0;return e.textContent=e.textContent.replace(/url\(('|")*#.+('|")*(?=\))/g,function(e){return s++,e+"-"+t}),s},f=function(e,t){var n,r,s,i,l,a,o,c,u,f,p,d,m,g,v,h,y=[{def:"linearGradient",attrs:["fill","stroke"]},{def:"radialGradient",attrs:["fill","stroke"]},{def:"clipPath",attrs:["clip-path"]},{def:"mask",attrs:["mask"]},{def:"filter",attrs:["filter"]},{def:"color-profile",attrs:["color-profile"]},{def:"cursor",attrs:["cursor"]},{def:"marker",attrs:["marker","marker-start","marker-mid","marker-end"]}];I.call(y,function(y){for(r=e.querySelectorAll(y.def+"[id]"),i=0,s=r.length;i<s;i++){for(n=r[i].id+"-"+t,c=y.attrs,f=0,u=c.length;f<u;f++)for(l=e.querySelectorAll("["+c[f]+'="url(#'+r[i].id+')"]'),o=0,a=l.length;o<a;o++)l[o].setAttribute(c[f],"url(#"+n+")");for(p=e.querySelectorAll("[*|href]"),g=[],m=0,d=p.length;m<d;m++)p[m].getAttributeNS(F,"href").toString()==="#"+r[i].id&&g.push(p[m]);for(h=0,v=g.length;h<v;h++)g[h].setAttributeNS(F,"href","#"+n);r[i].id=n}})},d=function(e,t,n){var r;"undefined"==typeof n&&(n=["id","viewBox"]);for(var s=0;s<e.attributes.length;s++)r=e.attributes.item(s),n.indexOf(r.name)<0&&t.setAttribute(r.name,r.value)},m=function(e){var n=t.createElementNS(V,"svg");return I.call(e.childNodes,function(e){n.appendChild(e.cloneNode(!0))}),d(e,n),n},g=function(e,t,n){var r,s,i,l,a,o,c=n.getAttribute("data-src").split("#"),u=e.textContent,f="",p=function(e,t,n){n[t]="."+i+" "+e};if(c.length>1)s=c[1],i=s+"-"+t,r=new RegExp("\\."+s+" ","g"),e.textContent=u.replace(r,"."+i+" ");else{for(l=c[0].split("/"),i=l[l.length-1].replace(".svg","")+"-"+t,r=new RegExp("([\\s\\S]*?){([\\s\\S]*?)}","g");null!==(a=r.exec(u));){o=a[1].trim().split(", "),o.forEach(p);var d=o.join(", ")+"{"+a[2]+"}\n";f+=d}e.textContent=f}n.setAttribute("class",n.getAttribute("class")+" "+i)},v=function(e){var t=e.getAttribute("class");return t?t.trim().split(" "):[]},h=function(e){var t=v(e),n="";return I.call(t,function(e){e.indexOf(a.spriteClassIdName)>=0&&(n=e.replace(a.spriteClassIdName,""))}),n},y=function(e,t,n){var r,s,i,l,a,o,c,u=!1,f=null;if(void 0===n)return t.cloneNode(!0);if(r=t.getElementById(n)){if(l=r.getAttribute("viewBox"),i=l.split(" "),r instanceof SVGSymbolElement)s=m(r),u=!0;else if(r instanceof SVGViewElement){if(f=null,e.onlyInjectVisiblePart){var p='*[width="'+i[2]+'"][height="'+i[3]+'"]';a={},0===Math.abs(parseInt(i[0]))?p+=":not([x])":(a.x=i[0],p+='[x="'+i[0]+'"]'),0===Math.abs(parseInt(i[1]))?p+=":not([y])":(a.y=i[1],p+='[y="'+i[1]+'"]'),c=t.querySelectorAll(p),c.length>1,f=c[0]}if(f&&f instanceof SVGSVGElement){s=f.cloneNode(!0);for(var d in a)"width"!==d&&"height"!==d&&s.removeAttribute(d)}else if(f&&f instanceof SVGUseElement){var g=t.getElementById(f.getAttributeNS(F,"href").substr(1));s=m(g),l=g.getAttribute("viewBox"),i=l.split(" "),u=!0}else u=!0,s=t.cloneNode(!0)}u&&(s.setAttribute("viewBox",i.join(" ")),s.setAttribute("width",i[2]+"px"),s.setAttribute("height",i[3]+"px")),s.setAttribute("xmlns",V),s.setAttribute("xmlns:xlink",F),o=v(s);var h=e.prefixFragIdClass+n;return o.indexOf(h)<0&&(o.push(h),s.setAttribute("class",o.join(" "))),s}},b=function(e,t,n,r){i[e]=i[e]||[],i[e].push({callback:n,fragmentId:t,element:r,name:name})},S=function(e){for(var t,n=0,r=i[e].length;n<r;n++)!function(n){setTimeout(function(){t=i[e][n],N(e,t.fragmentId,t.callback,t.element,t.name)},0)}(n)},A=function(t,n,s){var i,l,a,c,u;if(i=n.split("#"),l=i[0],a=2===i.length?i[1]:void 0,"undefined"!=typeof a&&(u=l.split("/"),c=u[u.length-1].replace(".svg","")),void 0!==r[l])r[l]instanceof SVGSVGElement?N(l,a,t,s,c):b(l,a,t,s,c);else{if(!e.XMLHttpRequest)return t("Browser does not support XMLHttpRequest"),!1;r[l]={},b(l,a,t,s,c);var f=new XMLHttpRequest;f.onreadystatechange=function(){if(4===f.readyState){if(404===f.status||null===f.responseXML)return t("Unable to load SVG file: "+l),!1;if(!(200===f.status||o.isLocal&&0===f.status))return t("There was a problem injecting the SVG: "+f.status+" "+f.statusText),!1;if(f.responseXML instanceof Document)r[l]=f.responseXML.documentElement;else if(DOMParser&&DOMParser instanceof Function){var e;try{var s=new DOMParser;e=s.parseFromString(f.responseText,"text/xml")}catch(t){e=void 0}if(!e||e.getElementsByTagName("parsererror").length)return t("Unable to parse SVG file: "+n),!1;r[l]=e.documentElement}S(l)}},f.open("GET",l),f.overrideMimeType&&f.overrideMimeType("text/xml"),f.send()}},C=function(e){var n="svg."+e+" {fill: currentColor;}",r=t.head||t.getElementsByTagName("head")[0],s=t.createElement("style");s.type="text/css",s.styleSheet?s.styleSheet.cssText=n:s.appendChild(t.createTextNode(n)),r.appendChild(s)},x=function(e,t,n){n?e.className.replace(t,""):e.className+=" "+t},N=function(t,n,i,o,c){var u,m,h,b;if(u=y(a,r[t],n),"undefined"==typeof u||"string"==typeof u)return i(u),!1;u.setAttribute("role","img"),I.call(u.children||[],function(e){e instanceof SVGDefsElement||e instanceof SVGTitleElement||e instanceof SVGDescElement||e.setAttribute("role","presentation")}),b=o.getAttribute("aria-hidden")||u.getAttribute("aria-hidden"),h=T("desc",u,o,n,c,!b),m=T("title",u,o,n,c,!b),b?u.setAttribute("aria-hidden","true"):u.setAttribute("aria-labelledby",m+" "+h),d(o,u,["class"]);var S=[].concat(u.getAttribute("class")||[],"injected-svg",o.getAttribute("class")||[]).join(" ");u.setAttribute("class",k(S)),f(u,s.count,c),u.removeAttribute("xmlns:a");for(var A,C,x=u.querySelectorAll("script"),N=[],w=0,j=x.length;w<j;w++)C=x[w].getAttribute("type"),C&&"application/ecmascript"!==C&&"application/javascript"!==C||(A=x[w].innerText||x[w].textContent,N.push(A),u.removeChild(x[w]));if(N.length>0&&("always"===a.evalScripts||"once"===a.evalScripts&&!l[t])){for(var E=0,G=N.length;E<G;E++)new Function(N[E])(e);l[t]=!0}var V=u.querySelectorAll("style");I.call(V,function(e){var t=v(u),n=!0;(t.indexOf(a.removeStylesClass)>=0||a.removeAllStyles)&&t.indexOf(a.keepStylesClass)<0?e.parentNode.removeChild(e):(p(e,s.count,u,c)>0&&(n=!1),a.prefixStyleTags&&(g(e,s.count,u,c),n=!1),n&&(e.textContent+=""))}),o.parentNode.replaceChild(u,o),delete s.elements[s.elements.indexOf(o)],s.count++,i(u)},k=function(e){e=e.split(" ");for(var t={},n=e.length,r=[];n--;)t.hasOwnProperty(e[n])||(t[e[n]]=1,r.unshift(e[n]));return r.join(" ")},w=function(e){return!!(e&&e.constructor&&e.call&&e.apply)},j=function(e){return"[object Array]"===Object.prototype.toString.call(e)},E=function(e,t){var n=e.getAttribute("class");n=n?n:"",j(t)&&(t=t.join(" ")),t=n+" "+t,e.setAttribute("class",k(t))},I=Array.prototype.forEach||function(e,t){if(void 0===this||null===this||"function"!=typeof e)throw new TypeError;var n,r=this.length>>>0;for(n=0;n<r;++n)n in this&&e.call(t,this[n],n,this)},T=function(e,t,n,r,i){var l,a=r?r+"-":"";return a+=e+"-"+s.count,l=n.querySelector(e),l?G(e,t,l.textContent,a,t.firstChild):(l=t.querySelector(e),l?l.setAttribute("id",a):i?G(e,t,r,a,t.firstChild):a=""),a},G=function(e,n,r,s,i){var l,a=n.querySelector(e);return a&&a.parentNode.removeChild(a),l=t.createElementNS(V,e),l.appendChild(t.createTextNode(r)),l.setAttribute("id",s),n.insertBefore(l,i),l},n}();"object"==typeof angular?angular.module("svginjector",[]).provider("svgInjectorOptions",function(){var e={};return{set:function(t){e=t},$get:function(){return e}}}).factory("svgInjectorFactory",["svgInjectorOptions",function(e){return new n(e)}]).directive("svg",["svgInjectorFactory",function(e){var t=e.getConfig();return{restrict:"E",link:function(n,r,s){s.class&&0===s.class.indexOf(t.spriteClassIdName)?s.$observe("class",function(){e.inject(r[0])}):(s.dataSrc||s.src)&&e.inject(r[0])}}}]):"object"==typeof module&&"object"==typeof module.exports?module.exports=n:"function"==typeof define&&define.amd?define(function(){return n}):"object"==typeof e&&(e.SVGInjector=n)}(window,document);
//# sourceMappingURL=./dist/svg-injector.map.js
/**
 * Extend jquery with a scrollspy plugin.
 * This watches the window scroll and fires events when elements are scrolled into viewport.
 *
 * throttle() and getTime() taken from Underscore.js
 * https://github.com/jashkenas/underscore
 *
 * @author Copyright 2013 John Smart
 * @license https://raw.github.com/thesmart/jquery-scrollspy/master/LICENSE
 * @see https://github.com/thesmart
 * @version 0.1.2
 */
(function($) {
	
		var jWindow = $(window);
		var elements = [];
		var elementsInView = [];
		var isSpying = false;
		var ticks = 0;
		var offset = {
			top : 0,
			right : 0,
			bottom : 0,
			left : 0,
		}
	
		/**
		 * Find elements that are within the boundary
		 * @param {number} top
		 * @param {number} right
		 * @param {number} bottom
		 * @param {number} left
		 * @return {jQuery}		A collection of elements
		 */
		function findElements(top, right, bottom, left) {
			var hits = $();
			$.each(elements, function(i, element) {
				var elTop = element.offset().top,
					elLeft = element.offset().left,
					elRight = elLeft + element.width(),
					elBottom = elTop + element.height();
	
				var isIntersect = !(elLeft > right ||
					elRight < left ||
					elTop > bottom ||
					elBottom < top);
	
				if (isIntersect) {
					hits.push(element);
				}
			});
	
			return hits;
		}
	
		/**
		 * Called when the user scrolls the window
		 */
		function onScroll() {
			// unique tick id
			++ticks;
	
			// viewport rectangle
			var top = jWindow.scrollTop(),
				left = jWindow.scrollLeft(),
				right = left + jWindow.width(),
				bottom = top + jWindow.height();
	
			// determine which elements are in view
			var intersections = findElements(top+offset.top, right+offset.right, bottom+offset.bottom, left+offset.left);
			$.each(intersections, function(i, element) {
				var lastTick = element.data('scrollSpy:ticks');
				if (typeof lastTick != 'number') {
					// entered into view
					element.triggerHandler('scrollSpy:enter');
				}
	
				// update tick id
				element.data('scrollSpy:ticks', ticks);
			});
	
			// determine which elements are no longer in view
			$.each(elementsInView, function(i, element) {
				var lastTick = element.data('scrollSpy:ticks');
				if (typeof lastTick == 'number' && lastTick !== ticks) {
					// exited from view
					element.triggerHandler('scrollSpy:exit');
					element.data('scrollSpy:ticks', null);
				}
			});
	
			// remember elements in view for next tick
			elementsInView = intersections;
		}
	
		/**
		 * Called when window is resized
		*/
		function onWinSize() {
			jWindow.trigger('scrollSpy:winSize');
		}
	
		/**
		 * Get time in ms
	   * @license https://raw.github.com/jashkenas/underscore/master/LICENSE
		 * @type {function}
		 * @return {number}
		 */
		var getTime = (Date.now || function () {
			return new Date().getTime();
		});
	
		/**
		 * Returns a function, that, when invoked, will only be triggered at most once
		 * during a given window of time. Normally, the throttled function will run
		 * as much as it can, without ever going more than once per `wait` duration;
		 * but if you'd like to disable the execution on the leading edge, pass
		 * `{leading: false}`. To disable execution on the trailing edge, ditto.
		 * @license https://raw.github.com/jashkenas/underscore/master/LICENSE
		 * @param {function} func
		 * @param {number} wait
		 * @param {Object=} options
		 * @returns {Function}
		 */
		function throttle(func, wait, options) {
			var context, args, result;
			var timeout = null;
			var previous = 0;
			options || (options = {});
			var later = function () {
				previous = options.leading === false ? 0 : getTime();
				timeout = null;
				result = func.apply(context, args);
				context = args = null;
			};
			return function () {
				var now = getTime();
				if (!previous && options.leading === false) previous = now;
				var remaining = wait - (now - previous);
				context = this;
				args = arguments;
				if (remaining <= 0) {
					clearTimeout(timeout);
					timeout = null;
					previous = now;
					result = func.apply(context, args);
					context = args = null;
				} else if (!timeout && options.trailing !== false) {
					timeout = setTimeout(later, remaining);
				}
				return result;
			};
		};
	
		/**
		 * Enables ScrollSpy using a selector
		 * @param {jQuery|string} selector  The elements collection, or a selector
		 * @param {Object=} options	Optional.
												throttle : number -> scrollspy throttling. Default: 100 ms
												offsetTop : number -> offset from top. Default: 0
												offsetRight : number -> offset from right. Default: 0
												offsetBottom : number -> offset from bottom. Default: 0
												offsetLeft : number -> offset from left. Default: 0
		 * @returns {jQuery}
		 */
		$.scrollSpy = function(selector, options) {
			selector = $(selector);
			selector.each(function(i, element) {
				elements.push($(element));
			});
			options = options || {
				throttle: 100
			};
	
			offset.top = options.offsetTop || 0;
			offset.right = options.offsetRight || 0;
			offset.bottom = options.offsetBottom || 0;
			offset.left = options.offsetLeft || 0;
	
			var throttledScroll = throttle(onScroll, options.throttle || 100);
			var readyScroll = function(){
				$(document).ready(throttledScroll);
			};
	
			if (!isSpying) {
				jWindow.on('scroll', readyScroll);
				jWindow.on('resize', readyScroll);
				isSpying = true;
			}
	
			// perform a scan once, after current execution context, and after dom is ready
			setTimeout(readyScroll, 0);
	
			return selector;
		};
	
		/**
		 * Get or set options
		 * @param {Object=} options	Optional.
												offsetTop : number -> offset from top. Default: 0
												offsetRight : number -> offset from right. Default: 0
												offsetBottom : number -> offset from bottom. Default: 0
												offsetLeft : number -> offset from left. Default: 0
		 * @returns {Object=} options	The options
		 */
		$.scrollSpyOptions = function(options) {
			if (!options instanceof Array) {
				// getter
				return offset;
			}
			// setter
	
			offset.top = options.offsetTop || 0;
			offset.right = options.offsetRight || 0;
			offset.bottom = options.offsetBottom || 0;
			offset.left = options.offsetLeft || 0;
	
			return offset;
		};
	
		/**
		 * Disable ScrollSpy using a selector
		 * @param {jQuery|string} selector  The elements collection, or a selector
		 * @returns {jQuery}
		 */
		$.stopScrollSpy = function(selector, options) {
			selector = $(selector);
			selector.each(function(i, element) {
				var pos = elements.indexOf(element);
				elements.splice(pos, 1);
			});
	
	
			if (!elements.length) {
				jWindow.off('scroll');
				jWindow.off('resize');
				isSpying = false;
			}
	
			return selector;
		};
	
		/**
		 * Listen for window resize events
		 * @param {Object=} options						Optional. Set { throttle: number } to change throttling. Default: 100 ms
		 * @returns {jQuery}		$(window)
		 */
		$.winSizeSpy = function(options) {
			$.winSizeSpy = function() { return jWindow; }; // lock from multiple calls
			options = options || {
				throttle: 100
			};
			return jWindow.on('resize', throttle(onWinSize, options.throttle || 100));
		};
	
		/**
		 * Enables ScrollSpy on a collection of elements
		 * e.g. $('.scrollSpy').scrollSpy()
		 * @param {Object=} options	Optional.
												throttle : number -> scrollspy throttling. Default: 100 ms
												offsetTop : number -> offset from top. Default: 0
												offsetRight : number -> offset from right. Default: 0
												offsetBottom : number -> offset from bottom. Default: 0
												offsetLeft : number -> offset from left. Default: 0
		 * @returns {jQuery}
		 */
		$.fn.scrollSpy = function(options) {
			return $.scrollSpy($(this), options);
		};
	
		/**
		 * Disabled ScrollSpy on a collection of elements
		 * e.g. $('.scrollSpy').stopScrollSpy()
		 * @returns {jQuery}
		 */
		$.fn.stopScrollSpy = function() {
			return $.stopScrollSpy($(this));
		};
	
	})(jQuery);
!function($){"use strict";var r=$("[data-paroller-factor]"),t={bgVertical:function(r,t){return r.css({"background-position":"center "+-t+"px"})},bgHorizontal:function(r,t){return r.css({"background-position":-t+"px center"})},vertical:function(r,t){return r.css({"-webkit-transform":"translateY("+t+"px)","-moz-transform":"translateY("+t+"px)",transform:"translateY("+t+"px)"})},horizontal:function(r,t){return r.css({"-webkit-transform":"translateX("+t+"px)","-moz-transform":"translateX("+t+"px)",transform:"translateX("+t+"px)"})}};$.fn.paroller=function(o){var a=$(window).height(),n=$(document).height(),o=$.extend({factor:0,type:"background",direction:"vertical"},o);r.each(function(){var r=$(this),e=r.offset().top,i=r.outerHeight(),c=r.data("paroller-factor"),l=r.data("paroller-type"),s=r.data("paroller-direction"),u=c?c:o.factor,f=l?l:o.type,d=s?s:o.direction,p=Math.round(e*u),h=Math.round((e-a/2+i)*u);"background"==f?"vertical"==d?t.bgVertical(r,p):"horizontal"==d&&t.bgHorizontal(r,p):"foreground"==f&&("vertical"==d?t.vertical(r,h):"horizontal"==d&&t.horizontal(r,h)),$(window).on("scroll",function(){var o=$(this).scrollTop();p=Math.round((e-o)*u),h=Math.round((e-a/2+i-o)*u),"background"==f?"vertical"==d?t.bgVertical(r,p):"horizontal"==d&&t.bgHorizontal(r,p):"foreground"==f&&n>o&&("vertical"==d?t.vertical(r,h):"horizontal"==d&&t.horizontal(r,h))})})}}(jQuery);
var partnerSwiper = new Swiper ('.partner-swiper', {
    // Optional parameters
    direction: 'horizontal',
    loop: true,
    slidesPerView: 4,
    centeredSlides: true,
    paginationClickable: true,
    spaceBetween: 250,
    autoplay: 1,
    speed: 3000,
    loopedSlides: 4,
    freeMode: true,
    freeModeMomentum: false,
    simulateTouch: false,
    watchSlidesVisibility: true,
    breakpoints: {
    // when window width is <= 320px
    320: {
      slidesPerView: 2,
      spaceBetween: 20
    },
    // when window width is <= 480px
    480: {
      slidesPerView: 2,
      spaceBetween: 25
    },
    // when window width is <= 640px
    640: {
      slidesPerView: 3,
      spaceBetween: 30
    },
    // when window width is <= 640px
    991: {
      slidesPerView: 3,
      spaceBetween: 35
    }

  }
})

jQuery(function($) {
  $( window ).on("resize", function(){
    partnerSwiper;
  });
});
jQuery(function($) {

  var $root = $('html, body');
  var navHeight = $(".header-site").height();
  var nameAnchor = $('a[name] + div');

  function setscrollspyfn() {

    // for each element with the class 'color'
     nameAnchor.each( function(i, element) {
        // cache the jQuery object
        var $this = $(this);
        var position = $this.position();
        var anchorGet = $this.prev().attr('name');

        $.scrollSpy(element).on('scrollSpy:enter', function() {
          $('.menu-item a[href$="' + anchorGet + '"]').addClass("selected");
          $('.menu-item a[href]').not($('.menu-item a[href$="' + anchorGet + '"]')).removeClass('selected');
        });
    });
  }

setscrollspyfn();


//   $(window).scroll(function() {
//     if (varble == true) {
//     clearTimeout(culo);
//     varble = false;
//     console.log(varble, " inside if");
//   }
// });

// // 1. Call helloCatAsync passing a callback function,
// //    which will be called receiving the result from the async operation
// helloCatAsync(function(result) {
//     // 5. Received the result from the async function,
//     //    now do whatever you want with it:
//     alert(result);
// });

// // 2. The "callback" parameter is a reference to the function which
// //    was passed as argument from the helloCatAsync call
// function helloCatAsync(callback) {
//     // 3. Start async operation:
//     setTimeout(function() {
//         // 4. Finished async operation,
//         //    call the callback passing the result as argument
//         callback('Nya');
//     }, Math.random() * 2000);
// }


  $('.menu-item a').not($('.menu-item-319 a')).click( function(event, cb) {

    event.preventDefault();
    var moduleName = $('[name="' + $(this).attr('href').substr(1) + '"]');
    var moduleOffset = ( moduleName.offset().top - navHeight );

    setTimeout(( function() {
        $root.animate({
        scrollTop: moduleOffset  },
        500
      );

    }), 250);



    $('.menu-item a').not(this).removeClass('selected');
    $(this).addClass("selected");

    if ( $( window ).width() < 992 ) {
      $('#off-canvas-right').removeClass('open');
    }
  });
  
  

  $('#layers-widget-slide-1 .copy-container .button').click( function(event) {

    event.preventDefault();
    var moduleName = $('[name="' + $(this).attr('href').substr(1) + '"]');
    var moduleOffset = (moduleName.offset().top) - navHeight;

    setTimeout((function() {
        $root.animate({
        scrollTop: moduleOffset  },
        500
      );
    }), 250);
  
  });


 

});

// Elements to inject 
var mySVGsToInject = document.querySelectorAll('svg.inject-me');

// Do the injection 
new SVGInjector().inject(mySVGsToInject);

jQuery(function($) {
    $( window ).on("load", function(){

        var divheading = $(".detail-page-content h3.heading, #layers-widget-slide-1 .swiper-slide h3.heading");

        divheading.after("<div class=\"underlining\"></div>");

        var divunderline = $(".detail-page-content .underlining, #layers-widget-slide-1 .swiper-slide h3.heading + .underlining");

        var i = 0;

        $.fn.textWidth = function(){
                
            var html_org = $(this).html();
            var html_calc = '<span>' + html_org + '</span>';
            $(this).html(html_calc);
            var width = $(this).find('span:first').width();
            $(this).html(html_org);
            return width;
                
        };

        divheading.each( function() {

            var txtwidth = $(this).textWidth();

            function underlineTitle() {
                divunderline.eq(i).css("width", txtwidth + "px");
            };

            underlineTitle();

            i++;

        });

        $( window ).on("resize", function(){

            var i = 0;
                
            divheading.each( function() {

                var txtwidth = $(this).textWidth();
                

                function underlineTitle() {
                    divunderline.eq(i).css("width", txtwidth + "px");
                };

                underlineTitle();

                i++;

            });

        });

    });
});
jQuery(function($) {
    $( window ).on("load", function(){
        
        $(window).paroller();

        // $(".svg-element, #rect-one, #rect-two").each(function() {
        //     $(this).paroller();
        // });
        // $(window).paroller({ factor: '0.5', type: 'foreground', direction: 'horizontal' });

    });
});