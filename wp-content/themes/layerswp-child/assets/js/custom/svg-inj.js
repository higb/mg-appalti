// Elements to inject 
var mySVGsToInject = document.querySelectorAll('svg.inject-me');

// Do the injection 
new SVGInjector().inject(mySVGsToInject);
