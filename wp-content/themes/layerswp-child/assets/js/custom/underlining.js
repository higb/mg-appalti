jQuery(function($) {
    $( window ).on("load", function(){

        var divheading = $(".detail-page-content h3.heading, #layers-widget-slide-1 .swiper-slide h3.heading");

        divheading.after("<div class=\"underlining\"></div>");

        var divunderline = $(".detail-page-content .underlining, #layers-widget-slide-1 .swiper-slide h3.heading + .underlining");

        var i = 0;

        $.fn.textWidth = function(){
                
            var html_org = $(this).html();
            var html_calc = '<span>' + html_org + '</span>';
            $(this).html(html_calc);
            var width = $(this).find('span:first').width();
            $(this).html(html_org);
            return width;
                
        };

        divheading.each( function() {

            var txtwidth = $(this).textWidth();

            function underlineTitle() {
                divunderline.eq(i).css("width", txtwidth + "px");
            };

            underlineTitle();

            i++;

        });

        $( window ).on("resize", function(){

            var i = 0;
                
            divheading.each( function() {

                var txtwidth = $(this).textWidth();
                

                function underlineTitle() {
                    divunderline.eq(i).css("width", txtwidth + "px");
                };

                underlineTitle();

                i++;

            });

        });

    });
});