<?php
/**
 * Template Name: Layers MG Detail Page Modal
 * The template for displaying a page
 *
 * @package Layers
 * @since Layers 1.0.0
 */


?>


<?php get_template_part( 'partials/header' , 'page-title' ); ?>

<div id="post-<?php the_ID(); ?>" <?php post_class( 'container content-main clearfix' ); ?>>
    <?php if( have_posts() ) : ?>
        <?php while( have_posts() ) : the_post(); ?>
            <div class="grid">
                <div class="column span-12">
                    <?php get_template_part( 'partials/content', 'single' ); ?>
                </div>
            </div>
        <?php endwhile; // while has_post(); ?>
    <?php endif; // if has_post() ?>
</div>

do_action('after_layers_builder_widgets');