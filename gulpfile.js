var gulp = require('gulp');
var concat = require('gulp-concat');  
var rename = require('gulp-rename');  
var uglify = require('gulp-uglify');


var jsFiles = 
    [
    'bower_components/svg-injector-2/dist/svg-injector.min.js',
    //'bower_components/swiper/dist/js/swiper.min.js',
    //'bower_components/jquery-modal/jquery.modal.min.js',
    //'jquery-scrollspy-master/jquery-scrollspy.min.js',
    'bower_components/jquery-scrollspy-thesmart/scrollspy.js',
    'bower_components/paroller.js/dist/jquery.paroller.min.js',
    'wp-content/themes/layerswp-child/assets/js/custom/partner.js',
    'wp-content/themes/layerswp-child/assets/js/custom/smooth-scroll.js',
    'wp-content/themes/layerswp-child/assets/js/custom/svg-inj.js',
    'wp-content/themes/layerswp-child/assets/js/custom/underlining.js',
    'wp-content/themes/layerswp-child/assets/js/custom/parallax-paroller.js'
    ];  
    
var jsDest = './wp-content/themes/layerswp-child/assets/js';

var cssFiles = 
    ['bower_components/swiper/dist/css/swiper.min.css',
    'bower_components/jquery-modal/jquery.modal.min.css'
    ];

var cssDest = './wp-content/themes/layerswp-child/assets/sass';


gulp.task('scripts', function() {  
    return gulp.src(jsFiles)
        .pipe(concat('mgAppalti.js'))
        .pipe(gulp.dest(jsDest))
        .pipe(rename('mgAppalti.min.js'))
        // .pipe(uglify())
        .pipe(gulp.dest(jsDest));
});


gulp.task('css', ['scripts'], function() {  
    return gulp.src(cssFiles)
        .pipe(concat('vendors.scss'))
        .pipe(gulp.dest(cssDest));
});