<?php
/**
 * Il file base di configurazione di WordPress.
 *
 * Questo file viene utilizzato, durante l’installazione, dallo script
 * di creazione di wp-config.php. Non è necessario utilizzarlo solo via
 * web, è anche possibile copiare questo file in «wp-config.php» e
 * riempire i valori corretti.
 *
 * Questo file definisce le seguenti configurazioni:
 *
 * * Impostazioni MySQL
 * * Prefisso Tabella
 * * Chiavi Segrete
 * * ABSPATH
 *
 * È possibile trovare ultetriori informazioni visitando la pagina del Codex:
 *
 * @link https://codex.wordpress.org/it:Modificare_wp-config.php
 *
 * È possibile ottenere le impostazioni per MySQL dal proprio fornitore di hosting.
 *
 * @package WordPress
 */

// ** Impostazioni MySQL - È possibile ottenere queste informazioni dal proprio fornitore di hosting ** //
/** Il nome del database di WordPress */
define('DB_NAME', 'mg-appalti-db');

/** Nome utente del database MySQL */
define('DB_USER', 'gblocal');

/** Password del database MySQL */
define('DB_PASSWORD', 'gblocal');

/** Hostname MySQL  */
define('DB_HOST', 'localhost');

/** Charset del Database da utilizzare nella creazione delle tabelle. */
define('DB_CHARSET', 'utf8mb4');

/** Il tipo di Collazione del Database. Da non modificare se non si ha idea di cosa sia. */
define('DB_COLLATE', '');

/**#@+
 * Chiavi Univoche di Autenticazione e di Salatura.
 *
 * Modificarle con frasi univoche differenti!
 * È possibile generare tali chiavi utilizzando {@link https://api.wordpress.org/secret-key/1.1/salt/ servizio di chiavi-segrete di WordPress.org}
 * È possibile cambiare queste chiavi in qualsiasi momento, per invalidare tuttii cookie esistenti. Ciò forzerà tutti gli utenti ad effettuare nuovamente il login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '<bp3rJ[^6In=<F9`0#Uu^g?K<)*rT:JmC*mmsc/lI%d6_LL^_+.7zVY}.:?:C$KR');
define('SECURE_AUTH_KEY',  '9S}aI=MKCp{[:L*odSQLKp2cAx<Ghgk:DG|pfjS3.>s=AH8^6|eNQ[[ ,7AW>JM$');
define('LOGGED_IN_KEY',    '<3,+`8[eO^)n(A>|&pO[Qm=8R$eQXgO1.j*@u-<)JYw^Exu<4/{;Q39ljm`gK+RQ');
define('NONCE_KEY',        'T1~Y3YyciL+q v6FurddORI`k-AR8_/&MWtfoDj:FoppCoAv5}0(lPU$_xiDO,_(');
define('AUTH_SALT',        '_>pP5:#Q0ig<n7x;+&PeFTPx=%Y4!p[xdwApqe}*Nt@FRm]o<1@Zg#{O=KQ#}tm*');
define('SECURE_AUTH_SALT', '+oSr%x,iEz%UDWX}!XG&Z[OJU:FvqBkC0%[7U#%l9Mszs;2H.9>M!QpFFplpzd^O');
define('LOGGED_IN_SALT',   'x?j@,IcwfN[Dk/2%0 B|U.BNW<N^)<O3HYSamRVKG2(PhTKz01kbMK+pf3zrQ] +');
define('NONCE_SALT',       '6q3]t}bH!`PW?Q~=.g_LBBmxVU&^|yp/RD>i=ZG#:a-Wes5Z5{8mYnZD5MN<{d%B');

/**#@-*/

/**
 * Prefisso Tabella del Database WordPress.
 *
 * È possibile avere installazioni multiple su di un unico database
 * fornendo a ciascuna installazione un prefisso univoco.
 * Solo numeri, lettere e sottolineatura!
 */
$table_prefix  = 'wp_';

/**
 * Per gli sviluppatori: modalità di debug di WordPress.
 *
 * Modificare questa voce a TRUE per abilitare la visualizzazione degli avvisi
 * durante lo sviluppo.
 * È fortemente raccomandato agli svilupaptori di temi e plugin di utilizare
 * WP_DEBUG all’interno dei loro ambienti di sviluppo.
 */
define('WP_DEBUG', false);

/* Finito, interrompere le modifiche! Buon blogging. */

/** Path assoluto alla directory di WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Imposta le variabili di WordPress ed include i file. */
require_once(ABSPATH . 'wp-settings.php');