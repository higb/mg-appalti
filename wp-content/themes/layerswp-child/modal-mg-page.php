<?php /* Template Name: MG Modal Page */ ?>

<?php
/**
 * The template for displaying a page
 *
 * @package Layers
 * @since Layers 1.0.0
 */
 ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
	<?php wp_head(); ?>

	

</head>
<body <?php body_class(); ?>>
	<?php do_action( 'layers_before_site_wrapper' ); ?>
	<div <?php layer_site_wrapper_class(); ?>>

    <?php do_action( 'layers_before_header' ); ?>

    <!--menu content-->

    <?php do_action( 'layers_after_header' ); ?>

	<section id="wrapper-content" <?php layers_wrapper_class( 'wrapper_content', 'wrapper-content' ); ?>>


<div <?php post_class( 'container content-main clearfix' ); ?>>
	<?php do_action('layers_before_page_loop'); ?>
    <div class="grid">
        <?php if( have_posts() ) : ?>
            <?php while( have_posts() ) : the_post(); ?>
                <article id="post-<?php the_ID(); ?>" <?php layers_center_column_class(); ?>>
                    <?php get_template_part( 'partials/content', 'single' ); ?>
                </article>
            <?php endwhile; // while has_post(); ?>
        <?php endif; // if has_post() ?>
    </div>
    <?php do_action('layers_after_page_loop'); ?>
</div>


</section>


	</div><!-- END / MAIN SITE #wrapper -->
	<?php do_action( 'layers_after_site_wrapper' ); ?>


<?php wp_footer(); ?>

	<!-- Advanced Custom Fields tags -->
	<?php the_field('mg-appalti');?>

</body>
</html>